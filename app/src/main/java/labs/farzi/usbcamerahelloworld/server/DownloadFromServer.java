package labs.farzi.usbcamerahelloworld.server;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import java.util.Locale;

import labs.farzi.usbcamerahelloworld.apis.RetrofitDownloadImage;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static labs.farzi.usbcamerahelloworld.MainActivity.URL;

public class DownloadFromServer{

private static  int TEXT_STATUS=0;
	static void getRetrofitImage(final Context context, String cookie) {
		Retrofit retrofit = new Retrofit.Builder()
			                    .baseUrl(URL)
			                    .addConverterFactory(GsonConverterFactory.create())
			                    .build();
		RetrofitDownloadImage service = retrofit.create(RetrofitDownloadImage.class);
		Call<ResponseBody> call = service.getImageDetails(cookie);
		call.enqueue(new Callback<ResponseBody>() {
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

				try {
					Log.d("onResponse", "Response came from server");
//					boolean FileDownloaded = DownloadImage(response.body());
					final String pred = response.body().string();
					Log.e("PREDICTION2", "onResponse: prediction = "+pred);
//					LinearLayout lView = new LinearLayout(context);
//					TextView myText = new TextView(context);
//					myText.setText(pred);
//
//					lView.addView(myText);
				final TextToSpeech textToSpeech=new TextToSpeech(context, new TextToSpeech.OnInitListener() {
					@Override
					public void onInit(int i) {
					if (i==TextToSpeech.ERROR){
						TEXT_STATUS=1;
						Toast.makeText(context, "Problem with vocals", Toast.LENGTH_LONG).show();
					}
					}
				});
					if(TEXT_STATUS!=1){
						new Thread(new Runnable() {
							@Override
							public void run() {
								textToSpeech.setLanguage(Locale.UK);
								textToSpeech.speak(pred, TextToSpeech.QUEUE_FLUSH, null);
							}
						}).start();

					}

				Toast.makeText(context, pred,
						Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Log.d("onResponse", "There is an error");
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t) {
				Log.d("onFailure", t.toString());
			}

			public void onResponseBody(Response<ResponseBody> response, Retrofit retrofit) {
				try {
					Log.e("onResponse", "Response came from server in SECOND");
				} catch (Exception e) {
					Log.d("onResponse", "There is an error");
					e.printStackTrace();
				}
			}

			public void onFailure(Throwable t) {
				Log.d("onFailure", t.toString());
			}
		});
	}

}
